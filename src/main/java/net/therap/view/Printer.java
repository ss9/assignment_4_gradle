package net.therap.view;

import net.therap.domain.Item;
import net.therap.domain.Menu;
import net.therap.helper.Constants;
import net.therap.helper.Helper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * @author shadman
 * @since 11/9/17
 */
public class Printer implements Serializable {

    public static final long serialVersionUID = 1L;

    private Scanner scanner;

    public Printer() {
        this.scanner = new Scanner(System.in);
    }

    public int getWhatToDo() {
        System.out.println(Constants.VIEW_OR_UPDATE_PROMPT);
        while (true) {
            if (scanner.hasNextInt()) {
                int whatToDo = scanner.nextInt();
                scanner.nextLine();
                switch (whatToDo) {
                    case Constants.VIEW:
                    case Constants.UPDATE:
                        return whatToDo;
                }
            }
            scanner.nextLine();
        }
    }

    public int getWhatToView() {
        while (true) {
            System.out.println(Constants.VIEWING_OPTIONS_PROMPT);
            if (scanner.hasNextInt()) {
                int whatToView = scanner.nextInt();
                switch (whatToView) {
                    case Constants.VIEW_MENU:
                    case Constants.VIEW_ITEMS:
                        return whatToView;
                }
            }
            scanner.nextLine();
        }
    }

    public int getWhichMenuToView() {
        System.out.println(Constants.MENU_TYPE_PROMPT);
        while (true) {
            if (scanner.hasNextInt()) {
                int whichMenuToView = scanner.nextInt();
                switch (whichMenuToView) {
                    case Constants.VIEW_BREAKFAST_MENU:
                    case Constants.VIEW_LUNCH_MENU:
                        return whichMenuToView;
                }
            }
            scanner.nextLine();
        }
    }

    public int getUpdateOperation() {
        while (true) {
            System.out.println(Constants.UPDATING_OPTIONS_PROMPT);
            if (scanner.hasNextInt()) {
                int updateOperation = scanner.nextInt();
                switch (updateOperation) {
                    case Constants.CREATE_ITEM:
                    case Constants.UPDATE_ITEM:
                    case Constants.DELETE_ITEM:
                    case Constants.CREATE_MENU:
                    case Constants.UPDATE_MENU:
                    case Constants.DELETE_MENU:
                        return updateOperation;
                }
            }
            scanner.nextLine();
        }
    }

    public List<Object> getNewItemAttributes() {
        List<Object> newItemAttributes = new ArrayList<>();
        System.out.println(Constants.CREATE_ITEM_NAME);
        while (true) {
            if (scanner.hasNextLine()) {
                String itemName = scanner.nextLine();
                if (itemName instanceof String && itemName.length() > 0) {
                    newItemAttributes.add(itemName);
                    break;
                }
            }
        }
        return newItemAttributes;
    }

    public int getIdOfItemToBeUpdated() {
        System.out.println(Constants.UPDATE_ITEM_PROMPT);
        while (true) {
            if (scanner.hasNextInt()) {
                int idOfItemToBeUpdated = scanner.nextInt();
                return idOfItemToBeUpdated;
            }
            scanner.nextLine();
        }
    }

    public int getCreateMenuDay() {
        System.out.println(Constants.CREATE_MENU_DAY_PROMPT);
        while (true) {
            if (scanner.hasNextInt()) {
                int createMenuDay = scanner.nextInt();
                if (Helper.INT_TO_DAY.containsKey(createMenuDay)) {
                    return createMenuDay;
                }
            }
            scanner.nextLine();
        }
    }

    public List<Integer> getCreateMenuItems(Map<Integer, Boolean> existingItems) {
        List<Integer> createMenuItems = new ArrayList<>();
        System.out.println(Constants.CREATE_MENU_ITEMS_PROMPT);
        while (true) {
            if (scanner.hasNextInt()) {
                Integer createMenuItem = scanner.nextInt();
                if (createMenuItem == 0) {
                    break;
                } else if (existingItems.containsKey(createMenuItem)) {
                    createMenuItems.add(createMenuItem);
                }
            }
            scanner.nextLine();
        }
        return createMenuItems;
    }

    public int getCreateMenuServingType() {
        System.out.println(Constants.CREATE_MENU_SERVING_TYPE_PROMPT);
        while (true) {
            if (scanner.hasNextInt()) {
                int createMenuServingType = scanner.nextInt();
                switch (createMenuServingType) {
                    case Constants.CREATE_BREAKFAST_MENU:
                    case Constants.CREATE_LUNCH_MENU:
                        return createMenuServingType;
                }
            }
            scanner.nextLine();
        }
    }

    public int getUpdateMenuDay() {
        System.out.println(Constants.UPDATE_MENU_DAY_PROMPT);
        while (true) {
            if (scanner.hasNextInt()) {
                int updateMenuDay = scanner.nextInt();
                if (Helper.INT_TO_DAY.containsKey(updateMenuDay)) {
                    return updateMenuDay;
                }
            }
            scanner.nextLine();
        }
    }

    public int getUpdateMenuServingType() {
        System.out.println(Constants.UPDATE_MENU_SERVING_TYPE_PROMPT);
        while (true) {
            if (scanner.hasNextInt()) {
                int updateMenuServingType = scanner.nextInt();
                switch (updateMenuServingType) {
                    case Constants.UPDATE_BREAKFAST_MENU:
                    case Constants.UPDATE_LUNCH_MENU:
                        return updateMenuServingType;
                }
            }
            scanner.nextLine();
        }
    }

    public void viewItems(List<Item> itemList) {
        System.out.printf("%20s%20s\n\n", "ID", "NAME");
        for (Item item : itemList) {
            System.out.printf("%20s%20s\n", item.getId(), item.getName());
        }
        System.out.println();
    }

    public void viewMenu(List<Menu> menuList) {
        for (Menu menu : menuList) {
            if (menu != null) {
                System.out.print(Helper.INT_TO_DAY.get(menu.getDay()) + " ");
                for (Item item : menu.getItemSet()) {
                    if (item instanceof Item) {
                        System.out.print(item.getName() + " ");
                    }
                }
                System.out.println("(" + Helper.INT_TO_SERVING_TIME.get(menu.getServingType()) + ")");
            }
        }
    }
}
