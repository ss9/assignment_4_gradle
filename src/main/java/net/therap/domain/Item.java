package net.therap.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author shadman
 * @since 11/15/17
 */

@Entity
@Table(name = "items")
public class Item implements Serializable {

    public static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 50, nullable = false)
    private String name;

    @ManyToMany(mappedBy = "itemSet", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<Menu> menuSet = new HashSet<>();

    public Item() {

    }

    public Item(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Menu> getMenuSet() {
        return menuSet;
    }

    public void setMenuSet(Set<Menu> menu) {
        this.menuSet = menuSet;
    }
}
