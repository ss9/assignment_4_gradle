package net.therap.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author shadman
 * @since 11/16/17
 */

@Embeddable
public class MenuPrimaryKey implements Serializable {

    public static final long serialVersionUID = 1l;

    private int day;
    private int servingType;

    public MenuPrimaryKey() {

    }

    public MenuPrimaryKey(int day, int servingType) {
        this.day = day;
        this.servingType = servingType;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getServingType() {
        return servingType;
    }

    public void setServingType(int servingType) {
        this.servingType = servingType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MenuPrimaryKey that = (MenuPrimaryKey) o;
        if (day != that.day) {
            return false;
        }
        if (servingType != that.servingType) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = day;
        result = 31 * result + servingType;
        return result;
    }
}
