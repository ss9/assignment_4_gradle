package net.therap.helper;

/**
 * @author shadman
 * @since 11/9/17
 */
public interface Constants {
    String CREATE_MENU_ITEMS_PROMPT = "Enter the ids of the items you want, 0 when done";
    String VIEW_OR_UPDATE_PROMPT = "Press 1 to view, 2 to update";
    String CREATE_ITEM_NAME = "Type the new item name";
    String UPDATE_ITEM_PROMPT = "Type the id of the item to be updated/deleted";
    String CREATE_MENU_DAY_PROMPT = "Press 1 to create menu for Sunday, 2 for Monday...7 for Saturday";
    String CREATE_MENU_SERVING_TYPE_PROMPT = "Press 1 to create breakfast menu, 2 to create lunch menu";
    String UPDATE_MENU_DAY_PROMPT = "Press 1 to update/delete menu for Sunday, 2 for Monday...7 for Saturday";
    String UPDATE_MENU_SERVING_TYPE_PROMPT = "Press 1 to update/delete breakfast menu, 2 to update/delete lunch menu";
    String MENU_TYPE_PROMPT = "Press 1 to view weekly breakfast menu, 2 to view weekly lunch menu";
    String VIEWING_OPTIONS_PROMPT = "Press 1 to view weekly menu, 2 to view all items";
    String UPDATING_OPTIONS_PROMPT = "Press 1 to create item, 2 to update item, 3 to delete item, "
            + "4 to create menu, 5 to update menu, 6 to delete menu";
    int VIEW = 1;
    int UPDATE = 2;
    int VIEW_MENU = 1;
    int VIEW_ITEMS = 2;
    int CREATE_ITEM = 1;
    int UPDATE_ITEM = 2;
    int DELETE_ITEM = 3;
    int CREATE_MENU = 4;
    int UPDATE_MENU = 5;
    int DELETE_MENU = 6;
    int CREATE_BREAKFAST_MENU = 1;
    int CREATE_LUNCH_MENU = 2;
    int UPDATE_BREAKFAST_MENU = 1;
    int UPDATE_LUNCH_MENU = 2;
    int VIEW_BREAKFAST_MENU = 1;
    int VIEW_LUNCH_MENU = 2;
}
